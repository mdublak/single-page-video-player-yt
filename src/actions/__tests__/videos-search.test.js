import { videosSearch } from '../videos-search';
import {
  VIDEOS_SEARCH_INIT,
  VIDEOS_SEARCH_SUCCESS,
  VIDEOS_SEARCH_ERROR,
} from '../types';
import { youtubeSearch } from 'utils/youtube-search';

jest.mock('utils/youtube-search', () => ({
  youtubeSearch: jest.fn(),
}));

const dispatch = jest.fn();

const term = 'keyword';

const mockResponse = 'Some response';

const mockError = 'Some error';

describe('videosSearch action', () => {
  describe('on success response', () => {
    beforeEach(() => {
      youtubeSearch.mockReturnValue(Promise.resolve(mockResponse));
      videosSearch(term)(dispatch);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should call youtubeSearch', () => {
      expect(youtubeSearch).toHaveBeenCalledWith(term);
    });

    it('should dispatch VIDEOS_SEARCH_INIT', () => {
      expect(dispatch.mock.calls[0][0]).toEqual({
        type: VIDEOS_SEARCH_INIT,
      });
    });

    it('should dispatch VIDEOS_SEARCH_SUCCESS', () => {
      expect(dispatch.mock.calls[1][0]).toEqual({
        type: VIDEOS_SEARCH_SUCCESS,
        videosList: mockResponse,
      });
    });
  });

  describe('on error response', () => {
    beforeEach(() => {
      youtubeSearch.mockReturnValue(Promise.reject(mockError));
      videosSearch(term)(dispatch);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should dispatch VIDEOS_SEARCH_INIT', () => {
      expect(dispatch.mock.calls[0][0]).toEqual({
        type: VIDEOS_SEARCH_INIT,
      });
    });

    it('should dispatch VIDEOS_SEARCH_ERROR', () => {
      expect(dispatch.mock.calls[1][0]).toEqual({
        type: VIDEOS_SEARCH_ERROR,
        error: mockError,
      });
    });
  });
});
