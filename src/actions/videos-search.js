import {
  VIDEOS_SEARCH_INIT,
  VIDEOS_SEARCH_SUCCESS,
  VIDEOS_SEARCH_ERROR,
} from './types';
import { youtubeSearch } from 'utils/youtube-search';

const videosSearchInit = () => ({
  type: VIDEOS_SEARCH_INIT,
});

const videosSearchSuccess = videosList => ({
  type: VIDEOS_SEARCH_SUCCESS,
  videosList,
});

const videosSearchError = error => ({
  type: VIDEOS_SEARCH_ERROR,
  error,
});

export const videosSearch = term => dispatch => {
  dispatch(videosSearchInit());

  return youtubeSearch(term)
    .then(response => dispatch(videosSearchSuccess(response)))
    .catch(error => dispatch(videosSearchError(error)));
};
