import React from 'react';
import { Provider } from 'react-redux';
import { store } from 'store';
import { mount } from 'enzyme';

import Container from '../Container';

jest.mock(
  '../Main',
  () =>
    function Main() {
      return null;
    },
);

const initState = {
  videosList: null,
  searchError: null,
  searching: false,
};

describe('<Connect(Main) /> (videos search)', () => {
  let componentRootElement;
  let wrapper;
  let componentProps;

  wrapper = mount(
    <Provider store={store}>
      <Container />
    </Provider>,
  );

  componentRootElement = wrapper.find('Main');
  componentProps = componentRootElement.props();

  it('should pass videosList prop', () => {
    expect(componentProps.videosList).toBe(initState.videosList);
  });
  it('should pass searchError prop', () => {
    expect(componentProps.searchError).toBe(initState.searchError);
  });
  it('should pass searching prop', () => {
    expect(componentProps.searching).toBe(initState.searching);
  });

  it('should pass videosSearch action', () => {
    expect(componentProps.actions.videosSearch).toBeInstanceOf(
      Function,
    );
  });
});
