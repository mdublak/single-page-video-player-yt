import React from 'react';
import { shallow } from 'enzyme';
import Main from '../Main';

const mockVideoList = [
  {
    id: { videoId: 'videoId1' },
    snippet: {
      videoDetails: 'Video details',
    },
  },
  {
    id: { videoId: 'videoId2' },
    snippet: {
      videoDetails: 'Video details2',
    },
  },
];
const mockError = { error: 'Some error' };
const mockVideosSearch = jest.fn();

const props = {
  actions: { videosSearch: mockVideosSearch },
};

describe('<Main />', () => {
  let wrapper;

  it('should match snapshot', () => {
    wrapper = shallow(<Main {...props} />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when searching', () => {
    wrapper = shallow(<Main {...props} searching />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when videosList', () => {
    wrapper = shallow(<Main {...props} videosList={mockVideoList} />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when error', () => {
    wrapper = shallow(<Main {...props} searchError={mockError} />);

    expect(wrapper).toMatchSnapshot();
  });
});
