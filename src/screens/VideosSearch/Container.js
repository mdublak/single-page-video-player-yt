import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { videosSearch } from 'actions/videos-search';

import Main from './Main';

const mapStateToProps = state => {
  return {
    videosList: state.videosSearch.videosList,
    searchError: state.videosSearch.searchError,
    searching: state.videosSearch.searching,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      videosSearch,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
