import React from 'react';
import { pathOr, isEmpty, head, isNil } from 'ramda';
import {
  func,
  bool,
  oneOfType,
  object,
  shape,
  array,
} from 'prop-types';
import SearchForm from 'components/SearchForm/search-form';
import Message from 'components/Message/message';
import VideoPlayer from 'components/VideoPlayer/video-player';
import VideoDetails from 'components/VideoDetails/video-details';

import './videos-search.scss';

const Main = ({
  actions: { videosSearch },
  videosList,
  searchError,
  searching,
}) => {
  const activeVideo = head(videosList || []);
  const activeVideoId = pathOr('', ['id', 'videoId'], activeVideo);
  const activeVideoDetails = pathOr({}, ['snippet'], activeVideo);

  const isMessage =
    isNil(videosList) ||
    isEmpty(videosList) ||
    (!isEmpty(searchError) && !isNil(searchError)) ||
    searching;
  const isVideo = !isEmpty(activeVideoId);

  return (
    <div className="videos-search">
      <SearchForm videosSearch={videosSearch} searching={searching} />
      {isMessage && (
        <Message
          videosList={videosList}
          searching={searching}
          error={searchError}
        />
      )}
      {isVideo && (
        <VideoPlayer videoId={activeVideoId} showControls />
      )}
      {!isEmpty(activeVideoDetails) && (
        <VideoDetails videoDetails={activeVideoDetails} />
      )}
    </div>
  );
};

Main.propTypes = {
  actions: shape({
    videosSearch: func.isRequired,
  }).isRequired,
  videosList: oneOfType([() => null, array]),
  searchError: oneOfType([() => null, object]),
  searching: bool,
};

Main.defaultProps = {
  videosList: null,
  searchError: null,
  searching: false,
};

export default Main;
