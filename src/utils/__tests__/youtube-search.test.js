import axios from 'axios';
import {
  API_KEY,
  API_SEARCH_URL,
  SEARCH_PART,
  SEARCH_TYPE,
  SEARCH_MAX_RESULTS,
} from '../const';
import { youtubeSearch } from '../youtube-search';

jest.mock('axios');

const data = 'Some data';
const error = 'Some error';

describe('youtubeSearch', () => {
  it('should return data from an API', async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(data));
    await expect(youtubeSearch('keyword')).resolves.toEqual(data);

    expect(axios.get).toHaveBeenCalledWith(API_SEARCH_URL, {
      params: {
        key: API_KEY,
        part: SEARCH_PART,
        type: SEARCH_TYPE,
        maxResults: SEARCH_MAX_RESULTS,
        q: 'keyword',
      },
    });
  });

  it('should return error from an API', async () => {
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(error)),
    );
    await expect(youtubeSearch('react')).rejects.toThrow(error);
  });
});
