import axios from 'axios';
import {
  API_KEY,
  API_SEARCH_URL,
  SEARCH_PART,
  SEARCH_TYPE,
  SEARCH_MAX_RESULTS,
} from './const';

export const youtubeSearch = term =>
  axios.get(API_SEARCH_URL, {
    params: {
      key: API_KEY,
      part: SEARCH_PART,
      type: SEARCH_TYPE,
      maxResults: SEARCH_MAX_RESULTS,
      q: term,
    },
  });
