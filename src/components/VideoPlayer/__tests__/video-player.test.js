import React from 'react';
import { shallow } from 'enzyme';
import VideoPlayer from '../video-player';
import VideoControls from '../video-controls';

const mockInitialState = {
  playing: false,
  pause: false,
};

describe('<VideoPlayer />', () => {
  let wrapper;

  it('should match snapshot when playing', () => {
    wrapper = shallow(<VideoPlayer videoId="qwerty" />);
    expect(wrapper).toMatchSnapshot();
  });

  describe('componentDidUpdate', () => {
    it('should set state to initial values when videoId update', () => {
      wrapper = shallow(<VideoPlayer videoId="abcdef" />);
      wrapper.setState({ playing: true });
      expect(wrapper.instance().state.playing).toBeTruthy();
      expect(wrapper.instance().state.pause).toBeFalsy();

      wrapper.setProps({ videoId: 'qwerty' });
      expect(wrapper.instance().state).toEqual(mockInitialState);
    });
  });

  describe('<VideoControls />', () => {
    it('should render "<VideoControls />" when showControls', () => {
      wrapper = shallow(<VideoPlayer videoId="abcdef" />);
      expect(wrapper.find(VideoControls).exists()).toBeFalsy();

      wrapper.setProps({ showControls: true });
      expect(wrapper.find(VideoControls).exists()).toBeTruthy();
    });

    describe('handlers', () => {
      let controls;
      const props = {
        videoId: '12ab34cd',
        showControls: true,
      };

      beforeEach(() => {
        wrapper = shallow(<VideoPlayer {...props} />);
        wrapper.setState({ mockInitialState });
        controls = wrapper.find(VideoControls);
      });

      it('should set state playing onPlay', () => {
        expect(wrapper.instance().state.playing).toBeFalsy();

        controls.simulate('play');
        expect(wrapper.instance().state.playing).toBeTruthy();
      });

      it('should set state pause onPause and when playing is set', () => {
        expect(wrapper.instance().state.playing).toBeFalsy();

        controls.simulate('pause');
        expect(wrapper.instance().state.pause).toBeFalsy();

        wrapper.setState({ playing: true });
        controls.simulate('pause');
        expect(wrapper.instance().state.pause).toBeTruthy();
      });
    });
  });
});
