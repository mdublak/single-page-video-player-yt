import React from 'react';
import { shallow } from 'enzyme';
import VideoControls from '../video-controls';

const props = {
  onPlay: jest.fn(),
  onPause: jest.fn(),
  playing: false,
  pause: false,
};

describe('<VideoControls />', () => {
  let wrapper;
  let button;

  it('should match snapshot when playing', () => {
    wrapper = shallow(<VideoControls {...props} playing />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when pause', () => {
    wrapper = shallow(<VideoControls {...props} pause />);
    expect(wrapper).toMatchSnapshot();
  });

  describe('handlers', () => {
    beforeEach(() => {
      wrapper = shallow(<VideoControls {...props} />);
    });

    it('should call onPlay when click', () => {
      button = wrapper.find('[data-test-id="play-button"]');
      button.simulate('click');
      expect(props.onPlay).toHaveBeenCalledTimes(1);
    });

    it('should call onPause when click', () => {
      button = wrapper.find('[data-test-id="pause-button"]');
      button.simulate('click');
      expect(props.onPause).toHaveBeenCalledTimes(1);
    });
  });
});
