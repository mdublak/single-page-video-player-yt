import React from 'react';
import { bool, func } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPlayCircle as faPlayCircleRegular,
  faPauseCircle as faPauseCircleRegular,
} from '@fortawesome/free-regular-svg-icons';
import {
  faPlayCircle as faPlayCircleSolid,
  faPauseCircle as faPauseCircleSolid,
} from '@fortawesome/free-solid-svg-icons';
library.add(
  faPlayCircleRegular,
  faPlayCircleSolid,
  faPauseCircleRegular,
  faPauseCircleSolid,
);

const VideoControls = ({ onPlay, onPause, playing, pause }) => (
  <div className="video-player__buttons">
    <button
      className="button button__play"
      onClick={onPlay}
      title="Play video"
      data-test-id="play-button"
    >
      <FontAwesomeIcon
        icon={[playing ? 'fas' : 'far', 'play-circle']}
      />
    </button>
    <button
      className="button button__pause"
      onClick={onPause}
      title="Pause video"
      data-test-id="pause-button"
    >
      <FontAwesomeIcon
        icon={[pause ? 'fas' : 'far', 'pause-circle']}
      />
    </button>
  </div>
);

VideoControls.propTypes = {
  onPlay: func.isRequired,
  onPause: func.isRequired,
  playing: bool.isRequired,
  pause: bool.isRequired,
};

export default VideoControls;
