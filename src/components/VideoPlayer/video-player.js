import React, { Component } from 'react';
import { bool, string } from 'prop-types';
import YouTubePlayer from 'react-player';
import VideoControls from './video-controls';

import './video-player.scss';

class VideoPlayer extends Component {
  state = {
    playing: false,
    pause: false,
  };

  componentDidUpdate(prevProps) {
    if (this.props.videoId !== prevProps.videoId) {
      this.setState({
        playing: false,
        pause: false,
      });
    }
  }

  handlePlay = () =>
    this.setState({
      playing: true,
      pause: false,
    });

  handlePause = () =>
    this.state.playing &&
    this.setState({
      playing: false,
      pause: true,
    });

  render() {
    const {
      props: { videoId, showControls },
      state: { playing, pause },
      handlePlay,
      handlePause,
    } = this;

    const videoUrl = `https://www.youtube.com/watch?v=${videoId}`;

    return (
      <div className="video-player">
        <div className="container container--absolute">
          <YouTubePlayer
            className="youtube-player fill-container"
            width="100%"
            height="100%"
            controls
            url={videoUrl}
            playing={this.state.playing}
            onPlay={this.handlePlay}
            onPause={this.handlePause}
          />
        </div>
        {showControls && (
          <VideoControls
            onPlay={handlePlay}
            onPause={handlePause}
            playing={playing}
            pause={pause}
          />
        )}
      </div>
    );
  }
}

VideoPlayer.propTypes = {
  videoId: string.isRequired,
  showControls: bool,
};

VideoPlayer.defaultProps = {
  showControls: false,
};

export default VideoPlayer;
