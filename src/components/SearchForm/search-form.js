import React, { Component } from 'react';
import { func, bool } from 'prop-types';
import { isEmpty } from 'ramda';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './search-form.scss';

class SearchForm extends Component {
  state = {
    term: '',
  };

  handleChange = e => this.setState({ term: e.target.value });

  handleSubmit = e => {
    e.preventDefault();

    const {
      state: { term },
      props: { searching },
    } = this;

    if (isEmpty(term) || searching) {
      return;
    }

    this.props.videosSearch(term);
  };

  render() {
    const {
      state: { term },
      handleChange,
      handleSubmit,
    } = this;

    return (
      <div className="search-form">
        <form onSubmit={handleSubmit}>
          <div className="search-form__input">
            <label htmlFor="search-input">
              <input
                id="search-input"
                name="search-input"
                type="search"
                placeholder="Search video"
                value={term}
                onChange={handleChange}
                data-test-id="search-input"
              />
            </label>
            <button
              className="button button__search"
              onClick={handleSubmit}
              title="Search video"
              data-test-id="search-button"
            >
              <FontAwesomeIcon icon={faSearch} />
            </button>
          </div>
        </form>
      </div>
    );
  }
}

SearchForm.propTypes = {
  videosSearch: func.isRequired,
  searching: bool,
};

SearchForm.defaultProps = {
  searching: false,
};

export default SearchForm;
