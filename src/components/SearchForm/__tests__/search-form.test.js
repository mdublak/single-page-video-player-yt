import React from 'react';
import { shallow } from 'enzyme';
import SearchForm from '../search-form';

const mockEvent = {
  preventDefault: jest.fn(),
  target: { value: 'keyword' },
};

const props = {
  videosSearch: jest.fn(),
  searching: false,
};

describe('<SearchForm />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<SearchForm {...props} />);
  });

  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  describe('handleChange', () => {
    it('should change term state when input change value', () => {
      wrapper.setState({ term: '' });
      expect(wrapper.instance().state.term).toBe('');

      wrapper
        .find('[data-test-id="search-input"]')
        .simulate('change', mockEvent);
      expect(wrapper.instance().state.term).toBe('keyword');
    });
  });

  describe('handleSubmit', () => {
    it('should not call videosSearch when term state is empty', () => {
      wrapper.setState({ term: '' });

      wrapper
        .find('[data-test-id="search-button"]')
        .simulate('click', mockEvent);
      expect(mockEvent.preventDefault).toHaveBeenCalledTimes(1);
      expect(props.videosSearch).toHaveBeenCalledTimes(0);
    });

    it('should not call videosSearch when searching', () => {
      wrapper.setState({ term: 'keyword' });
      wrapper.setProps({ searching: true });

      wrapper
        .find('[data-test-id="search-button"]')
        .simulate('click', mockEvent);
      expect(mockEvent.preventDefault).toHaveBeenCalledTimes(2);
      expect(props.videosSearch).toHaveBeenCalledTimes(0);
    });

    it('should call videosSearch', () => {
      wrapper.setState({ term: 'keyword' });

      wrapper
        .find('[data-test-id="search-button"]')
        .simulate('click', mockEvent);
      expect(mockEvent.preventDefault).toHaveBeenCalledTimes(3);
      expect(props.videosSearch).toHaveBeenCalledTimes(1);
      expect(props.videosSearch).toHaveBeenCalledWith('keyword');
    });
  });
});
