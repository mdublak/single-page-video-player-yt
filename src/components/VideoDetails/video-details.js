import React from 'react';
import { shape, string } from 'prop-types';

import './video-details.scss';

const VideoDetails = ({
  videoDetails: { title, description, channelTitle },
}) => (
  <div className="video-details">
    {title && (
      <h2
        className="video-details__title"
        dangerouslySetInnerHTML={{ __html: title }}
      />
    )}
    {description && (
      <p
        className="video-details__description"
        dangerouslySetInnerHTML={{ __html: description }}
      />
    )}
    {channelTitle && (
      <p className="video-details__channel-name">
        <strong>From:</strong> <i>{channelTitle}</i>
      </p>
    )}
  </div>
);

VideoDetails.propTypes = {
  videoDetails: shape({
    title: string,
    description: string,
    channelTitle: string,
  }),
};

VideoDetails.defaultProps = {
  videoDetails: {},
};

export default VideoDetails;
