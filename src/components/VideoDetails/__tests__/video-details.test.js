import React from 'react';
import { shallow } from 'enzyme';
import VideoDetails from '../video-details';

const mockVideoDetails = {
  title: 'Title',
  description: 'Description',
  channelTitle: 'Channel title',
};
const props = {
  videoDetails: mockVideoDetails,
};

describe('<VideoDetails />', () => {
  let wrapper;

  it('should match snapshot when data exist', () => {
    wrapper = shallow(<VideoDetails {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when data does not exist', () => {
    wrapper = shallow(<VideoDetails videoDetails={{}} />);
    expect(wrapper).toMatchSnapshot();
  });
});
