import React from 'react';
import { array, bool, object, oneOfType } from 'prop-types';
import { prop, isEmpty, isNil } from 'ramda';

import './message.scss';

const Message = ({ videosList, error, searching }) => {
  const messages = {
    welcome: {
      title: 'Welcom to our site!',
      description: 'Find a video you want to view.',
    },
    searching: {
      title: 'Searching...',
    },
    noResults: {
      title: 'No results!',
      description: 'Try again using different keywords.',
    },
    error: {
      title: 'Oops, something went wrong!',
      description: 'Please try again later.',
    },
  };

  const messageType = () => {
    if (searching) {
      return 'searching';
    }

    if (!isNil(error) && !isEmpty(error)) {
      return 'error';
    }

    if (!isNil(videosList) && isEmpty(videosList)) {
      return 'noResults';
    }

    return 'welcome';
  };

  const message = prop(messageType(), messages);

  return (
    <div className="container container--absolute">
      <div className="message fill-container">
        <p className="message__title">{message.title}</p>
        {message.description && (
          <p className="message__description">
            {message.description}
          </p>
        )}
      </div>
    </div>
  );
};

Message.propTypes = {
  videosList: oneOfType([() => null, array]),
  error: oneOfType([() => null, object]),
  searching: bool,
};

Message.defaultProps = {
  videosList: null,
  error: null,
  searching: false,
};

export default Message;
