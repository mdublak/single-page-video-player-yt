import React from 'react';
import { shallow } from 'enzyme';
import Message from '../message';

const mockVideosList = [];
const mockError = { error: 'Some error' };

describe('<Message />', () => {
  let wrapper;

  it('should match snapshot when default', () => {
    wrapper = shallow(<Message />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when searching', () => {
    wrapper = shallow(<Message searching />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when no results', () => {
    wrapper = shallow(<Message videosList={mockVideosList} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should match snapshot when error', () => {
    wrapper = shallow(<Message error={mockError} />);
    expect(wrapper).toMatchSnapshot();
  });
});
