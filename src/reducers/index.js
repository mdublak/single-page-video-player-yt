import { combineReducers } from 'redux';
import { videosSearch } from './videos-search';

export default combineReducers({
  videosSearch,
});
