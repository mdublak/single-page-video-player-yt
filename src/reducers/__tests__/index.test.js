import reducer from '../index';
import {
  videosSearch,
  initialState as videosSearchInitialState,
} from '../videos-search';
import { createStore } from 'redux';

let store = createStore(reducer);

describe('reducer', () => {
  it('should return default state if no action type is recognized', () => {
    expect(reducer({}, { type: null })).toEqual({
      videosSearch: videosSearchInitialState,
    });
  });

  it('should contain videosSearch logic', () => {
    expect(store.getState().videosSearch).toEqual(
      videosSearch(undefined, { type: null }),
    );
  });
});
