import { videosSearch, initialState } from '../videos-search';
import {
  VIDEOS_SEARCH_INIT,
  VIDEOS_SEARCH_SUCCESS,
  VIDEOS_SEARCH_ERROR,
} from 'actions/types';
import deepFreeze from 'deep-freeze';

const defaultState = deepFreeze(initialState);

const mockVideosList = {
  data: {
    items: ['item'],
  },
};

const mockError = 'Some error';

describe('videosSearch reducer', () => {
  it('should return default state when no state passed', () => {
    expect(videosSearch()).toEqual(defaultState);
  });

  it('should set searching on VIDEOS_SEARCH_INIT', () => {
    const action = {
      type: VIDEOS_SEARCH_INIT,
    };

    expect(videosSearch(defaultState, action)).toEqual({
      ...defaultState,
      searching: true,
    });
  });

  it('should set videosList and disable searching on VIDEOS_SEARCH_SUCCESS', () => {
    const action = {
      type: VIDEOS_SEARCH_SUCCESS,
      videosList: mockVideosList,
    };

    expect(videosSearch(defaultState, action)).toEqual({
      ...defaultState,
      videosList: ['item'],
      searching: false,
    });
  });

  it('should set error and disable searching on VIDEOS_SEARCH_ERROR', () => {
    const action = {
      type: VIDEOS_SEARCH_ERROR,
      error: mockError,
    };

    expect(videosSearch(defaultState, action)).toEqual({
      ...defaultState,
      searchError: 'Some error',
      searching: false,
    });
  });
});
