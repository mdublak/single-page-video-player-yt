import {
  VIDEOS_SEARCH_INIT,
  VIDEOS_SEARCH_SUCCESS,
  VIDEOS_SEARCH_ERROR,
} from 'actions/types';
import { path } from 'ramda';

export const initialState = {
  videosList: null,
  searchError: null,
  searching: false,
};

export const videosSearch = (state = initialState, action = {}) => {
  switch (action.type) {
    case VIDEOS_SEARCH_INIT:
      return {
        ...initialState,
        searching: true,
      };
    case VIDEOS_SEARCH_SUCCESS:
      return {
        ...state,
        videosList: path(['data', 'items'], action.videosList),
        searching: false,
      };
    case VIDEOS_SEARCH_ERROR:
      return {
        ...state,
        searchError: action.error,
        searching: false,
      };
    default:
      return state;
  }
};
